package com.demo;


import org.apache.camel.FluentProducerTemplate;
import org.apache.camel.test.spring.CamelSpringBootRunner;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;

@SpringBootTest
@RunWith(CamelSpringBootRunner.class)
public class JavaScriptRouteTest {

  @Autowired
  FluentProducerTemplate producer;

  @Test
  public void testJsonTranslator() {

    File file = new File("data/input.txt");
    String result = producer
        .to("direct:jsonTranslator")
        .withHeader("JavaScriptFileName", "jsonTranslator1.js")
        .withBody(file)
        .request(String.class);

    System.out.printf("RESULT :\n%s", result);
  }
}