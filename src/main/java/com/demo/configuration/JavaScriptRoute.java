package com.demo.configuration;

import org.apache.camel.builder.RouteBuilder;

public class JavaScriptRoute extends RouteBuilder {

  @Override
  public void configure() throws Exception {

    from("file://data?fileName=input.txt&noop=true")
        .autoStartup(false)
        .routeId("fileRoute")
        .setHeader("JavaScriptFileName")
        .constant("jsonTranslator1.js")
        .to("direct:jsonTranslator");


    from("direct:jsonTranslator")
        .routeId("jsonTranslatorRoute")
        .setHeader("jsonTranslator")
        .simple("language://javaScript:classpath:com/demo/translator/${header.JavaScriptFileName}")
        .convertBodyTo(String.class)
        .toD("${header.JsonTranslator}");
  }
}
