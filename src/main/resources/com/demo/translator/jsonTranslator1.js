'use strict';

    /**

     * plat sample data
     * =>

     JSON 형태로 아래와 같이 body 키 값으로 전달됩니다.
     {
     "body":"5017501       0100000000001    7501I18050000009    UnitSwift   ROOT     011     103     I7501I18050000009      USD3087.6   AITHER    SWIFT   USDSHBKUSU60NYX    SHBKUSU60NYX    88    {1:F01SHBKUS60ANYX4723000471}{2:O1030430180525SHBKUS60ABYX47230001611805250430N}{3:{108:7501O18050000015}}{4: :20:810186990767 :23B:CRED :23E:PHOB/(919)-529-3133 :32A:170620USD3087,6 :50K:/741939618 - 1100 SMART POWER SYSTEM CO., LTD 3, ACHASAN-RO, SEONGDONG-GU, SEOUL NOTRH KOREA :57A:BOFAUS3NXXX :59:/237033494281 STAYONLINE 1506 IVAC WAY CREEDMOOR,NC 27522 :71A:SHA :72:/ACC/10101 //BANK OF AMERICA, N.A. //100 NOTRTH TRYON STREET CHARLOTTE //,NC 28255 //SWIFT CODE. BOFAUS3NXXX -}{5:{MAC:00006D7B}}{S:{SAC:}{NET:XIT}{XDT:1805251730}}"
     }
     **/

    // request.body 에서 데이타를 받아옴.
    var data = request.body;

    // console.log(data);

    // res JSON 데이타를 생성.
    var res = {};

    // data 에서 Fixed Text를 substr 로 잘라내서 저장함. 실구현시에는 공백처리를 위해서 공백까지 포함한 정확한 위치값 + trim 으로 가져와야 함.
    res.versionTag = data.substr(26, 1);
    res.messageID = data.substr(31, 16);
    res.unit = data.substr(51, 9);
    res.businessUnit = data.substr(63, 4);
    res.bypass = data.substr(72, 1);        // 011에서 가장 앞부분
    res.command = data.substr(73, 1);       // 011에서 중간
    res.businessType = data.substr(74, 1);  // 011에서 마지막
    res.messageType = data.substr(80, 1);   // 103 중에서 1만 가져옴
    res.ioIndicator = data.substr(88, 1);   // I7501I18050000009 중에서 I만 가져옴
    res.senderReference = data.substr(89, 16);
    res.currency = data.substr(111, 3); // USD3087.6 에서 USD만
    res.amount = data.substr(114, 9).trim();   // 실제로는 공백처리를 위해서 자릿수 + trim 으로 변환해서 가져와야함.
    res.applicationCode = data.substr(123, 6);
    res.userCode = "";
    res.messageFormat = data.substr(133, 5);
    res.additionalCurrency = data.substr(141, 3);
    res.senderCode = data.substr(144, 12);
    res.receiverCode = data.substr(160, 12);
    res.applicationPriority = data.substr(176, 2);
    res.cutoffTime = "";
    res.messageChecksum = "";
    res.systemID = "";
    res.nomalizedAmount = "";
    res.messageData = data.substr(182, data.length - 182); // MessageData는 시작위치~끝까지. 전체길이-시작위치로 끝까지의 길이를 가져옴.

    // 응답으로 res를 반환함.
    request.body = JSON.stringify(res, null, 2);


